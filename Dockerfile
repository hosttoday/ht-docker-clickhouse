FROM yandex/clickhouse-server
RUN apt-get update \
    && apt-get install curl -y \
    && curl -fsSL https://deb.nodesource.com/setup_18.x | bash - \
    && apt-get update \
    &&  apt-get install -y nodejs
RUN node -v && npm -v
